package jee6ejb.config;

import javax.enterprise.inject.Produces;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggingConfig {

    @Produces
    public Logger mainLogger(){
        return LoggerFactory.getLogger("jee6-ejb.MAIN");
    }
}
