package jee6ejb.service.impl;

import jee6ejb.dao.GenericDAO;
import jee6ejb.model.Personne;
import jee6ejb.service.PersonneService;
import java.util.Collection;
import javax.ejb.Singleton;
import javax.inject.Inject;

@Singleton
public class PersonneServiceImpl implements PersonneService {

    @Inject
    private GenericDAO dao;

    @Override
    public Collection<Personne> getAllPersonnes() {
        return dao.loadAll(Personne.class);
    }

    @Override
    public void savePersonne(Personne p) {
        dao.saveOrUpdate(p);
    }
}
