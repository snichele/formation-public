package jee6ejb.service;

import jee6ejb.model.Personne;
import java.util.Collection;

public interface PersonneService {

    Collection<Personne> getAllPersonnes();
    void savePersonne(Personne p);

}
