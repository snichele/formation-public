package jee6ejb.jsf2examples.wizard;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import lombok.Data;
import org.slf4j.LoggerFactory;

@ManagedBean
@SessionScoped
@Data
public class WizardBean {

    private static final int NOT_STARTED_STEP = 0;
    private static final int FINISHED_STEP = 4;
    private boolean currentWizardStarted;
    private boolean currentWizardCompleted;
    private int step = NOT_STARTED_STEP;

    private String transactionId;

    public void begin(ActionEvent e) {
        currentWizardStarted = true;
        nextStep();
    }

    void nextStep() {
        increaseStepByOne();
        checkWizardCompletnessAndFlagItAccordingly();
    }

    private void increaseStepByOne() {
        step++;
    }

    private void checkWizardCompletnessAndFlagItAccordingly() {
        if (step == FINISHED_STEP) {
            currentWizardCompleted = true;
        }
    }

    public void destroy(ActionEvent e){
        LoggerFactory.getLogger(WizardBean.class).info("Wizard Bean supposedly destroyed !");
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("wizardBean");
    }
}
