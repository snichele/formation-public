package jee6ejb.jsf2examples.wizard;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import lombok.Data;
import org.slf4j.LoggerFactory;

@ManagedBean
@ViewScoped
@Data
public class Step1Bean {

    private String name;
    private String surname;
    @ManagedProperty(value = "#{wizardBean}")
    private WizardBean associatedWizard;

    public Step1Bean() {
        LoggerFactory.getLogger(Step1Bean.class).info("Created a new Step 1 Bean !");
    }

    public void tryToValidateStepAndAdvanceWizardIfOk(ActionEvent ae) {
        // Perform real save in database
        boolean success = true;

        // Is successfull, tell the wizard that we can proceed !
        if (success) {
            associatedWizard.setTransactionId("AZDDE00789");
            associatedWizard.nextStep();
        }
    }
}
