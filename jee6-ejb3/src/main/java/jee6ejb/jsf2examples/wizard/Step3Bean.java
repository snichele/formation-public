package jee6ejb.jsf2examples.wizard;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import lombok.Data;
import org.slf4j.LoggerFactory;

@ManagedBean
@ViewScoped
@Data
public class Step3Bean {

    @ManagedProperty(value = "#{wizardBean}")
    private WizardBean associatedWizard;

    public Step3Bean() {
        LoggerFactory.getLogger(Step3Bean.class).info("Created a new Step 3 Bean !");
    }

    public void tryToValidateStepAndAdvanceWizardIfOk(ActionEvent ae) {
        // Perform real save in database
        boolean success = true;

        // Is successfull, tell the wizard that we can proceed !
        if (success) {
            associatedWizard.nextStep();
        }
    }
}
