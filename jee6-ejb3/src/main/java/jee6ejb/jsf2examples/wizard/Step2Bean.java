package jee6ejb.jsf2examples.wizard;

import com.google.common.base.Predicate;
import static com.google.common.collect.Collections2.*;
import static com.google.common.collect.Lists.*;
import java.util.Arrays;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import lombok.Data;
import org.slf4j.LoggerFactory;

@ManagedBean
@ViewScoped
@Data
public class Step2Bean {

    private String number;
    private String whatever;
    @ManagedProperty(value = "#{wizardBean}")
    private WizardBean associatedWizard;

    public Step2Bean() {
        LoggerFactory.getLogger(Step2Bean.class).info("Created a new Step 2 Bean !");
    }

    public void tryToValidateStepAndAdvanceWizardIfOk(ActionEvent ae) {
        // Perform real save in database
        boolean success = true;

        // Is successfull, tell the wizard that we can proceed !
        if (success) {
            associatedWizard.nextStep();
        }
    }

    public List<String> getAllPossibleWhatever(final String query) {
        List<String> fullList = getSampleList();
        return newArrayList(
                filter(fullList, new Predicate<String>() {
            @Override
            public boolean apply(String t) {
                return t.startsWith(query);
            }
        }));
    }

    private List<String> getSampleList() {
        return Arrays.asList("AZEAZEA", "AZEAZEAZE", "SDFSZDFSDF", "ERRGBERBERB", "AZEQSDERH");
    }
}
