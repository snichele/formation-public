package jee6ejb.dao.impl;

import jee6ejb.dao.GenericDAO;
import jee6ejb.model.Personne;
import java.io.Serializable;
import java.util.Collection;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Singleton
@Startup
public class JPAGenericDao implements GenericDAO {

    @PersistenceContext(unitName = "aston_jee6")
    private EntityManager entityManager;

    @Override
    public <T> Collection<T> loadAll(Class<T> ofClass) {
        return entityManager
                .createQuery("from " + ofClass.getSimpleName())
                .getResultList();
    }

    public Collection<Personne> loadAllPersons() {
        return entityManager.createQuery("from Personne").getResultList();
    }

    @Override
    public void saveOrUpdate(Object o) {
        entityManager.merge(o);
    }
}