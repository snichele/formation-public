package jee6ejb.dao;

import java.util.Collection;

public interface GenericDAO {

    public <T> Collection<T> loadAll(Class<T> ofClass);
    public void saveOrUpdate(Object o);
}
