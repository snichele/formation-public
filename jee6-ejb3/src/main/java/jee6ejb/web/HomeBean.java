package jee6ejb.web;

import jee6ejb.model.Personne;
import jee6ejb.service.PersonneService;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.Collection;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import org.slf4j.Logger;

@Named(value = "homeBean")
@SessionScoped
public class HomeBean implements Serializable {

    private String message = "EJB & JEE6 rocks !";
    @Inject
    private PersonneService personneService;

    @Inject
    private Logger mainLogger;

    private String firstName;
    private String lastName;

    public HomeBean() {
    }

    @PostConstruct
    void init() {
        mainLogger.info("HomeBean initialized");
        mainLogger.info("I'm gonna create some person in db...");
        createRandomPersonneInDB();
    }

    public String getMessage() {
        return message;
    }

    public Collection<Personne> getPersonnes() {
        return personneService.getAllPersonnes();
    }

    public void savePersonneInDB(ActionEvent actionEvent) {
        mainLogger.info("About to save a new Personne : {} {}", firstName, lastName);
        personneService.savePersonne(new Personne(firstName, lastName));
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("You've registered"));
    }

    private void createRandomPersonneInDB() {
        personneService.savePersonne(new Personne("Personne " + Math.random(), "" + Math.random()));
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
