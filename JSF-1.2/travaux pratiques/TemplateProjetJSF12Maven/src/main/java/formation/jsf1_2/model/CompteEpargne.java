package formation.jsf1_2.model;


public class CompteEpargne extends Compte {
    private Double tauxInterets;

    public CompteEpargne() {
        super();
    }

    public CompteEpargne(String libelle, Double solde, Double tauxInterets) {
        super(libelle, solde);
        this.tauxInterets = tauxInterets;
    }

    public Double getTauxInterets() {
        return tauxInterets;
    }

    public void setTauxInterets(Double tauxInterets) {
        this.tauxInterets = tauxInterets;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
