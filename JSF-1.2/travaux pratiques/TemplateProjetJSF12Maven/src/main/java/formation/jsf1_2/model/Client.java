package formation.jsf1_2.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Client implements Serializable {

    private Integer id;
    private String nom;
    private String login;
    private String password;
    private Set<CompteCourant> compteCourants;
    private Set<CompteEpargne> compteEpargnes;

    public Client() {
        this.compteCourants = new HashSet<CompteCourant>();
        this.compteEpargnes = new HashSet<CompteEpargne>();
    }

    public Client(String nom, String login, String password) {
        this();
        this.nom = nom;
        this.login = login;
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<CompteCourant> getCompteCourants() {
        return compteCourants;
    }

    public void setCompteCourants(Set<CompteCourant> compteCourants) {
        this.compteCourants = compteCourants;
    }

    public Set<CompteEpargne> getCompteEpargnes() {
        return compteEpargnes;
    }

    public void setCompteEpargnes(Set<CompteEpargne> compteEpargnes) {
        this.compteEpargnes = compteEpargnes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Client client = (Client) o;

        if (!id.equals(client.id)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        StringBuilder buffer = new StringBuilder();
        buffer.append("Client=[id='").append(this.id).append("']");
        return buffer.toString();
    }
}
