package formation.jsf1_2.model;

public class CompteCourant extends Compte {

    public CompteCourant() {
        super();
    }

    public CompteCourant(String libelle, Double solde) {
        super(libelle, solde);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
