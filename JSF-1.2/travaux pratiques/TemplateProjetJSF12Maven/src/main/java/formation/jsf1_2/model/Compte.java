package formation.jsf1_2.model;

import java.io.Serializable;

public abstract class Compte implements Serializable {

    private Integer id;
    private String libelle;
    private Double solde;

    public Compte() {
        super();
    }

    protected Compte(String libelle, Double solde) {
        this.libelle = libelle;
        this.solde = solde;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Double getSolde() {
        return solde;
    }

    public void setSolde(Double solde) {
        this.solde = solde;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Compte compte = (Compte) o;

        if (!id.equals(compte.id)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        StringBuilder buffer = new StringBuilder();
        buffer.append("Compte=[id='").append(this.id).append("']");
        return buffer.toString();
    }
}
