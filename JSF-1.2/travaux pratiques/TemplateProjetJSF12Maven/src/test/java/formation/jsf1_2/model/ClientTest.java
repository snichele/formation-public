package formation.jsf1_2.model;

import org.junit.Test;
import static org.junit.Assert.*;

public class ClientTest {

    @Test
    public void verify_correct_comptes_lists_initialization() {
        Client c = new Client();
        assertNotNull(c.getCompteCourants());
        assertNotNull(c.getCompteEpargnes());
        assertEquals(0, c.getCompteEpargnes().size());
    }
}
