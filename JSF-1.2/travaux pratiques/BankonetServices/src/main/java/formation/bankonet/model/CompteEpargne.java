package formation.bankonet.model;

import java.math.BigDecimal;

/**
 * Classe repr�sentant un compte �pargne (Livret A, PEL...). Un compte possède
 * un taux d'intéret et un plafond de crédit. Un compte ne peut présenter un
 * solde négatif.
 *
 * @see Compte
 * @author SQLI http://www.sqli.com
 */
public class CompteEpargne extends Compte {

    public static final String TYPE_COMPTE_EPARGNE = "CE";
    private BigDecimal tauxInteret;
    private BigDecimal plafondVersement;

    public CompteEpargne(int identifiant, String clientId, String libelle, BigDecimal solde, BigDecimal tauxInteret, BigDecimal plafondCredit) {
        super(identifiant, clientId, libelle, solde);
        this.tauxInteret = tauxInteret;
        this.plafondVersement = plafondCredit;
    }

    public boolean verifieCreditAutorise(BigDecimal montant) throws IllegalArgumentException {
        if (montant.compareTo(plafondVersement) < 1) {
            return true;
        }
        throw new IllegalArgumentException("Cr�dit refus�, le compte �pargne " + getId() + "//" + getLibelle() + " a pour plafond de versement : " + plafondVersement + ".");
    }

    public boolean verifieDebitAutorise(BigDecimal montant) throws IllegalArgumentException {
        if (getSolde().subtract(montant.abs()).compareTo(BigDecimal.ZERO) >= 0) {
            return true;
        }
        throw new IllegalArgumentException("D�bit refus�, le montant est trop �lev�, le solde du compte epargne " + getId() + "//" + getLibelle() + " ne peut �tre n�gatif.");
    }

    public BigDecimal getTauxInteret() {
        return tauxInteret;
    }

    public BigDecimal getPlafondVersement() {
        return plafondVersement;
    }

    @Override
    public boolean isOfType(String typeString) {
        return TYPE_COMPTE_EPARGNE.equals(typeString);
    }
}