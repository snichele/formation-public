package formation.bankonet.model;

import java.math.BigDecimal;

/**
 * Classe abstraite repr�sentant un compte g�n�rique.
 *
 * @author SQLI http://www.sqli.com
 */
public abstract class Compte {

    private final int id;
    private final String clientId;
    private final String libelle;
    private BigDecimal solde;

    public Compte(int identifiant, String clientId, String libelle, BigDecimal solde) {
        this.id = identifiant;
        this.clientId = clientId;
        this.libelle = libelle;
        this.solde = solde;
    }

    private boolean isNegative(BigDecimal montant) {
        return montant.signum() == -1;
    }

    private boolean isZero(BigDecimal montant) {
        return montant.signum() == 0;
    }

    private boolean isPositive(BigDecimal montant) {
        return montant.signum() == 1;
    }

    public void modifieSolde(BigDecimal montant) throws IllegalArgumentException {
        if (isNegative(montant)) {
            verifieDebitAutorise(montant);
        } else if (isPositive(montant)) {
            verifieCreditAutorise(montant);
        }
        if (!isZero(montant)) {
            this.solde = this.solde.add(montant);
        }
    }

    public abstract boolean verifieCreditAutorise(BigDecimal montant) throws IllegalArgumentException;

    public abstract boolean verifieDebitAutorise(BigDecimal montant) throws IllegalArgumentException;

    public abstract boolean isOfType(String typeString);

    public int getId() {
        return id;
    }

    public String getClientId() {
        return clientId;
    }

    public String getLibelle() {
        return libelle;
    }

    public BigDecimal getSolde() {
        return solde;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Compte) {
            Compte c = (Compte) o;
            if (this.id == c.id) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + this.id;
        return hash;
    }

    @Override
    public String toString() {
        return this.id + "//" + this.libelle;
    }
}