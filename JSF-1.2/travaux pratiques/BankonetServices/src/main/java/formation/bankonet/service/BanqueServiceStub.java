package formation.bankonet.service;

import formation.bankonet.model.Compte;
import formation.bankonet.model.CompteCourant;
import formation.bankonet.model.CompteEpargne;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class BanqueServiceStub implements BanqueService {

    private static final Map<String, List<? extends Compte>> COMPTES_BY_CLIENT_ID = new TreeMap<String, List<? extends Compte>>();
    private static final String CLIENT_ONE_ID = "4807-1358-9962-X";
    private static final String CLIENT_TWO_ID = "4807-1358-9856-C";

    static {
        COMPTES_BY_CLIENT_ID.put(
                CLIENT_ONE_ID,
                Arrays.asList(
                new CompteCourant(1, CLIENT_ONE_ID, "Mon premier compte courant", new BigDecimal(2000), new BigDecimal(1000)),
                new CompteCourant(2, CLIENT_ONE_ID, "Mon deuxième compte courant", new BigDecimal(350), new BigDecimal(1000)),
                new CompteEpargne(3, CLIENT_ONE_ID, "Mon premier compte épargne", new BigDecimal(350), new BigDecimal(5), new BigDecimal(1000)),
                new CompteEpargne(4, CLIENT_ONE_ID, "Mon deuxième compte épargne", new BigDecimal(10000), BigDecimal.TEN, new BigDecimal(10001)),
                new CompteEpargne(5, CLIENT_ONE_ID, "Mon compte épargne secret", new BigDecimal(100000), new BigDecimal(30), new BigDecimal(1000000))));
        COMPTES_BY_CLIENT_ID.put(
                CLIENT_TWO_ID,
                Arrays.asList(
                new CompteCourant(10, CLIENT_ONE_ID, "Mon premier compte courant", new BigDecimal(4578), new BigDecimal(100)),
                new CompteEpargne(13, CLIENT_ONE_ID, "Mon premier compte épargne", new BigDecimal(554), new BigDecimal(5), new BigDecimal(758))));
    }

    public String getClientIdForLogin(String login) {
        if("client1".equals(login)) {
            return CLIENT_ONE_ID;
        }
        if("client2".equals(login)) {
            return CLIENT_TWO_ID;
        }
        return CLIENT_ONE_ID;
    }

    public Compte getCompteById(int id) {
        for (Map.Entry<String, List<? extends Compte>> entry : COMPTES_BY_CLIENT_ID.entrySet()) {
            List<? extends Compte> list = entry.getValue();
            for (Compte compte : list) {
                if (compte.getId() == id) {
                    return compte;
                }
            }
        }
        return null;
    }

    public List<Compte> listerTousLesComptes() {
        List<Compte> allComptes = new ArrayList<Compte>();
        for (List<? extends Compte> list : COMPTES_BY_CLIENT_ID.values()) {
            allComptes.addAll(list);
        }
        return allComptes;
    }

    public List<Compte> listerCompteCourant(String clientId) {
        return Collections.unmodifiableList(filtrerComptesDeTypes(COMPTES_BY_CLIENT_ID.get(clientId), CompteCourant.TYPE_COMPTE_COURANT));
    }

    public List<Compte> listerCompteEpargne(String clientId) {
        return Collections.unmodifiableList(filtrerComptesDeTypes(COMPTES_BY_CLIENT_ID.get(clientId), CompteEpargne.TYPE_COMPTE_EPARGNE));
    }

    private List<Compte> filtrerComptesDeTypes(List<? extends Compte> comptes, String type) {
        List<Compte> comptesToReturn = new ArrayList<Compte>();
        if (comptes == null) {
            return comptesToReturn;
        }
        for (Compte compte : comptes) {
            if (compte.isOfType(type)) {
                comptesToReturn.add(compte);
            }
        }
        return comptesToReturn;
    }

    public int getCompteCourantCount(String clientId) {
        return listerCompteCourant(clientId).size();
    }

    public int getCompteEpargneCount(String clientId) {
        return listerCompteEpargne(clientId).size();
    }

    public void effectuerVirement(Compte source, Compte cible, BigDecimal montant) {
        throw new UnsupportedOperationException("Not supported yet !");
    }
}
