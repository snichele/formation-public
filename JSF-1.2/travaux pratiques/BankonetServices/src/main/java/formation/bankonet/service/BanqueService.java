package formation.bankonet.service;

import formation.bankonet.model.Compte;
import java.math.BigDecimal;
import java.util.List;
import java.util.ServiceLoader;

/**
 * Interface définissant les services d'une banque.
 *
 */
public interface BanqueService {

    String getClientIdForLogin(String login);

    Compte getCompteById(int id);

    List<Compte> listerTousLesComptes();
    
    List<Compte> listerCompteCourant(String clientId);

    List<Compte> listerCompteEpargne(String clientId);

    int getCompteCourantCount(String clientId);

    int getCompteEpargneCount(String clientId);

    void effectuerVirement(Compte source, Compte cible, BigDecimal montant);

    final class Provider {

        private Provider() {
        }

        public static BanqueService getDefault() {
            ServiceLoader<BanqueService> ldr = ServiceLoader.load(BanqueService.class);
            for (BanqueService provider : ldr) {
                return provider;
            }
            throw new IllegalStateException("No BanqueService registered");
        }
    }
}