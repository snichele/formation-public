package formation.bankonet.model;

import java.math.BigDecimal;

/**
 * Classe repr�sentant un compte courant. Un compte courant possède un découvert
 * autorisé.
 *
 * @see Compte
 * @author SQLI http://www.sqli.com
 */
public class CompteCourant extends Compte {

    public static final String TYPE_COMPTE_COURANT = "CC";
    private final BigDecimal decouvertAutorise;

    public CompteCourant(int identifiant, String clientId, String libelle, BigDecimal solde, BigDecimal decouvertAutorise) {
        super(identifiant, clientId, libelle, solde);
        this.decouvertAutorise = decouvertAutorise;
    }

    public boolean verifieCreditAutorise(BigDecimal montant) {
        return true;
    }

    public boolean verifieDebitAutorise(BigDecimal montant) throws IllegalArgumentException {
        if (getSolde().add(montant).compareTo(decouvertAutorise) < 0) {
            throw new IllegalArgumentException("D�bit refus�, le montant est trop �lev�, le solde du compte courant " + getId() + "//" + getLibelle() + " ne peut descendre en dessous du d�couvert autoris�.");
        }
        return true;
    }

    public BigDecimal getDecouvertAutorise() {
        return decouvertAutorise;
    }

    @Override
    public boolean isOfType(String typeString) {
        return TYPE_COMPTE_COURANT.equals(typeString);
    }
}