package formation.bankonet.model;

import java.math.BigDecimal;
import static org.junit.Assert.*;
import org.junit.Test;

public class CompteCourantTest {

    @Test
    public void test_compte_courant_alimentation_OK() {
        CompteCourant cc = createSampleCompteCourant();
        cc.modifieSolde(new BigDecimal(1000));
        assertEquals(cc.getSolde(), new BigDecimal(1010));
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_compte_courant_credit_KO() {
        CompteCourant cc = createSampleCompteCourant();
        cc.modifieSolde(new BigDecimal(-1000));
    }

    private CompteCourant createSampleCompteCourant() {
        return new CompteCourant(1, "1", "azeaze", BigDecimal.TEN, BigDecimal.ZERO);
    }
}
