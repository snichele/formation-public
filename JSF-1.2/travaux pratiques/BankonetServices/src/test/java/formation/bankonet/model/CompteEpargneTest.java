package formation.bankonet.model;

import formation.bankonet.model.CompteEpargne;
import java.math.BigDecimal;
import static org.junit.Assert.*;
import org.junit.Test;

public class CompteEpargneTest {

    @Test
    public void test_compte_epargne_alimentation_OK() {
        CompteEpargne cc = createSampleCompteEpargne();
        cc.modifieSolde(new BigDecimal(1000));
        assertEquals(cc.getSolde(), new BigDecimal(2000));
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_compte_epargne_alimentation_KO_plafond_depasse() {
        CompteEpargne cc = createSampleCompteEpargne();
        cc.modifieSolde(new BigDecimal(10000));
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_compte_epargne_credit_KO() {
        CompteEpargne cc = createSampleCompteEpargne();
        cc.modifieSolde(new BigDecimal(-1001));
    }

    private CompteEpargne createSampleCompteEpargne() {
        return new CompteEpargne(1, "1", "azeaze", new BigDecimal(1000), BigDecimal.TEN, new BigDecimal(9999));
    }
}
