package formation.jsf1_2.web;

import formation.bankonet.service.BanqueService;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;

public class LoginBeanTest {

    FacesContext currentTestMethodContext;

    @Before
    public void beforeTestMethod() {
        currentTestMethodContext = JSFContextMocker.mockFacesContext();
    }

    @After
    public void afterTestMethod() {
        currentTestMethodContext.release();
    }

    private LoginBean newLoginBeanInstanceWithMocksInjected(boolean withValidUserInfosPrefilled) {
        LoginBean bean = new LoginBean();
        if (withValidUserInfosPrefilled) {
            bean.setLogin("client1");
            bean.setPassword("client1");
        }
        final BanqueService banqueServiceMock = mock(BanqueService.class);
        when(banqueServiceMock.getClientIdForLogin("client1")).thenReturn("DUMMY_ID");
        bean.setBanqueService(banqueServiceMock);
        return bean;
    }

    @Test
    public void check_authenticate_failed_behaviour() {
        LoginBean bean = newLoginBeanInstanceWithMocksInjected(false);
        bean.setLogin("foo");
        bean.setPassword("foo");
        bean.authenticate();
        assertNull(bean.getCurrentUserClient());
        assertNull(bean.getClientId());
        verify(currentTestMethodContext).addMessage(null, LoginBean.LOGIN_FAILED_FACES_MESSAGE);
    }

    @Test
    public void check_authenticate_successfull_behaviour() {
        LoginBean bean = newLoginBeanInstanceWithMocksInjected(true);
        bean.authenticate();
        assertNotNull(bean.getCurrentUserClient());
        assertNotNull(bean.getClientId());
        verify(currentTestMethodContext, never()).addMessage(null, LoginBean.LOGIN_FAILED_FACES_MESSAGE);
    }

    @Test(expected = IllegalStateException.class)
    public void check_disconnect_invalid_call_behaviour() {
        LoginBean bean = newLoginBeanInstanceWithMocksInjected(false);
        bean.disconnect();
    }

    @Test
    public void check_disconnect_successfull_behaviour() {
        HttpSession fakeSession = configureMockedHttpSession();
        LoginBean bean = newLoginBeanInstanceWithMocksInjected(true);
        bean.authenticate();
        bean.disconnect();
        verify(fakeSession).invalidate();
    }

    private HttpSession configureMockedHttpSession() {
        HttpSession fakeSession = mock(HttpSession.class);
        ExternalContext ext = mock(ExternalContext.class);
        when(ext.getSession(false)).thenReturn(fakeSession);
        when(currentTestMethodContext.getExternalContext()).thenReturn(ext);
        return fakeSession;
    }
}
