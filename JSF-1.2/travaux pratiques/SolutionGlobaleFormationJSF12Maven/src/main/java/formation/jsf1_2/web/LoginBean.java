package formation.jsf1_2.web;

import formation.bankonet.model.Client;
import formation.bankonet.service.BanqueService;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoginBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginBean.class);
    static final FacesMessage LOGIN_FAILED_FACES_MESSAGE = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Login failed", "You login action failed !");
    private String login;
    private String password;
    private Client currentUserClient;
    private String clientId;
    private BanqueService banqueService;

    public LoginBean() {
        LOGGER.debug("New LoginBean instance created !");
    }

    public String authenticate() {
        if (("client1".equals(login) && "client1".equals(password))
                || ("client2".equals(login) && "client2".equals(password))) {
            currentUserClient = new Client("Un client", login, password);
            clientId = banqueService.getClientIdForLogin(login);
            LOGGER.info("{} user has just connected.", login);
            return "loginSuccessfull";
        }
        FacesContext.getCurrentInstance().addMessage(null, LOGIN_FAILED_FACES_MESSAGE);
        return "loginFailure";
    }

    public String disconnect() {
        if(currentUserClient == null) {
            throw new IllegalStateException("Cannot disconnect an unidentified user");
        }
        LOGGER.info("{} user has just disconnected.", login);
        ((javax.servlet.http.HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false)).invalidate();
        return "home";
    }

    public String getLogin() {
        return login;
    }

    public Client getCurrentUserClient() {
        return currentUserClient;
    }

    public String getClientId() {
        return clientId;
    }

    public void setLogin(String login) {
        LOGGER.trace("login property set to {}", login);
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        LOGGER.trace("password property set...");
        this.password = password;
    }

    public BanqueService getBanqueService() {
        return banqueService;
    }

    public void setBanqueService(BanqueService banqueService) {
        this.banqueService = banqueService;
    }
}