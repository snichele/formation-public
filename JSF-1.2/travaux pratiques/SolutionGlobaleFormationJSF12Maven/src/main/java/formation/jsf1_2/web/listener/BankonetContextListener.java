package formation.jsf1_2.web.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BankonetContextListener implements ServletContextListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(BankonetContextListener.class);

    
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        LOGGER.info(
                "\n _____             _                    _   \n"
                + "| ___ \\           | |                  | |  \n"
                + "| |_/ / __ _ _ __ | | _____  _ __   ___| |_ \n"
                + "| ___ \\/ _` | '_ \\| |/ / _ \\| '_ \\ / _ \\ __|\n"
                + "| |_/ / (_| | | | |   < (_) | | | |  __/ |_ \n"
                + "\\____/ \\__,_|_| |_|_|\\_\\___/|_| |_|\\___|\\__|\n"  
                );
        LOGGER.info("... has just started.");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        LOGGER.info("Bankonet application destroyed !");
    }
}
