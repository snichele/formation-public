package formation.jsf1_2.web.compte;

import formation.bankonet.model.Compte;
import formation.bankonet.service.BanqueService;
import formation.jsf1_2.web.LoginBean;
import java.util.ArrayList;
import java.util.List;
import javax.faces.event.ActionEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyAccountsBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(MyAccountsBean.class);
    private BanqueService banqueService;
    private DataModel dataModel;
    private Compte selectedAccount;
    private LoginBean loginBean;

    public MyAccountsBean() {
        LOGGER.debug("Created new instance of myAccountsBean");
    }

    public List<? extends Compte> getAllAccounts() {
        LOGGER.info("{} user has just consulted his accounts list.", loginBean.getLogin());
        final List<Compte> allAccounts = new ArrayList<Compte>();
        allAccounts.addAll(banqueService.listerCompteCourant(loginBean.getClientId()));
        allAccounts.addAll(banqueService.listerCompteEpargne(loginBean.getClientId()));
        return allAccounts;
    }

    public void updateSelected(ActionEvent event) {
        setSelectedAccount((Compte) dataModel.getRowData());
    }

    public BanqueService getBanqueService() {
        return banqueService;
    }

    public void setBanqueService(BanqueService banqueService) {
        this.banqueService = banqueService;
    }

    public DataModel getDataModel() {
        dataModel = new ListDataModel(getAllAccounts());
        return dataModel;
    }

    public Compte getSelectedAccount() {
        return selectedAccount;
    }

    public void setSelectedAccount(Compte selectedAccount) {
        LOGGER.info("{} user has just consulted his accounts detail for : {} ", loginBean.getLogin(), selectedAccount);        
        this.selectedAccount = selectedAccount;
    }

    public LoginBean getLoginBean() {
        return loginBean;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }
}
